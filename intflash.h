#ifndef __FLASH_H__
#define __FLASH_H__

#include <stdint.h>

#define FLASH_SECTOR_LEN 64

void flash_quick_erase(uint32_t addr);
void flash_quick_program(uint32_t addr, const uint8_t data[FLASH_SECTOR_LEN]);
void flash_quick_replace(uint32_t addr, const uint8_t data[FLASH_SECTOR_LEN]);

#endif
