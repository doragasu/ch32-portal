TARGET:=ch32-portal
CH32V003FUN:=lib/ch32v003fun/ch32v003fun
MINICHLINK?=lib/ch32v003fun/minichlink

ADDITIONAL_C_FILES+=intflash.c

all : $(TARGET).bin

include $(CH32V003FUN)/ch32v003fun.mk

flash : cv_flash
clean : cv_clean
