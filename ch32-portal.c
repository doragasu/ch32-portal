/* PINOUT:
 * - PD0: UART TX
 * - PD1: SWD
 * - PD3, PD4, PD5: USB (used only by bootloader)
 * - PD6: PCB LED (only used for debugging)
 * - PD7: NRST (no config needed)
 * - PC4: Blue button
 * - PC3: Orange button
 * - PC6: Pixels (configured by ws2812b module)
 *
 *
 * Read the README.md file for the juicy details!
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "ch32v003fun.h"

#define WS2812DMA_IMPLEMENTATION
#define WSGRB

// 20 LEDs, 1 for indicator, 17 for ring plus center, 2 for bar
#define NR_LEDS 20
// Must be divisible by 4
#define DMALEDS 24

#include "ws2812b_dma_spi_led_driver.h"
#include "intflash.h"

// SysTick uses the /8 prescaler
#define SYSTICK_CLK (FUNCONF_SYSTEM_CORE_CLOCK / 8)
// Baudrate for the DFPlayer Mini
#define UART_BR 9600

// Uart TX pin on port D
#define UART_TX 0
// PCB build-in LED (nanoCH32V003 board)
#define LED 6
// Buttons on port C
#define BUTTON_BLUE 4
#define BUTTON_ORAN 3

// RGB to uint32_t, applying configured intensity scaling
#define RGB2U32(r, g, b) ( \
		((uint32_t)((r)>>(8 - cfg.intensity))) |    \
		((uint32_t)((g)>>(8 - cfg.intensity))<<8) | \
		((uint32_t)((b)>>(8 - cfg.intensity))<<16)  \
		)
// Portal gun blue color (cyan)
#define COLOR_BLUE RGB2U32(0, 255, 255)
// Portal gun orange color
#define COLOR_ORAN RGB2U32(255, 102, 0)

// Gun sounds folder is "01", and must be shifted 8 times left
#define FOLDER_GUN 0x100
// Turret sounds folder is "02", and must be shifted 8 times left
#define FOLDER_TURRET 0x200
// GLaDOS sounds folder is "03", and must be shifted 8 times left
#define FOLDER_GLADOS 0x300

// Number of sound files in the turret folder
#define TURRET_NFILES 50
// Number of sound files in the GLaDOS folder
#define GLADOS_NFILES 78

// Delay to enter config mode on button hold
#define CFG_MODE_DELAY_MS 5000
// Delay after sending a volume change command
#define VOL_SET_DELAY_MS 150

// Set volume command. Minimum values is 0, maximum is 30
#define YX_VOL_SET_CMD 6
// Play file from folder command
#define YX_FOLDER_PLAY_CMD 15

// Reads specified key
#define KEY_READ(key) (GPIOC->INDR & (1<<(key)))
// Returns true if specified key is pressed
#define KEY_PRESSED(key) (0 == KEY_READ(key))

// Magic number: 67AD05CF (reminiscent of GLaDOS config)
#define CFG_MAGIC 0x67AD05CF
// Configuration offset, at the end of the 16 KiB flash
#define CFG_ADDR  (0x08000000 + 16384 - 64)
// Default sound volume when not configured
#define CFG_VOLUME_DEFAULT 15
// Default intensity when not configured
#define CFG_INTENSITY_DEFAULT 8

// Supported button events
enum event {
	EVT_NONE = 0,
	EVT_BUTTON_ORANGE_PRESS,
	EVT_BUTTON_BLUE_PRESS,
	EVT_BUTTON_ORANGE_RELEASE,
	EVT_BUTTON_BLUE_RELEASE,
	EVT_BUTTON_ORANGE_DOUBLE,
	EVT_BUTTON_BLUE_DOUBLE,
	EVT_BUTTON_ORANGE_LONG,
	EVT_BUTTON_BLUE_LONG,
	__EVT_MAX
};

// Working modes
enum mode {
	MODE_NORMAL,
	MODE_CFG_VOLUME,
	MODE_CFG_INTENSITY,
	__MODE_MAX
};

// Frame for sending data to the YX audio player chip
union yx_cmd_frame {
	struct {
		uint8_t sof;
		uint8_t ver;
		uint8_t len;
		uint8_t cmd;
		uint8_t feedback;
		uint8_t param_h;
		uint8_t param_l;
		uint8_t csum_h;
		uint8_t csum_l;
		uint8_t eof;
	};
	uint8_t data[10];
};

// Configuration saved to flash, must be 64-byte long
struct config {
	uint32_t magic;        // Magic number, set to CFG_MAGIC
	uint8_t volume;        // Volume from 0 (mute) to 30
	uint8_t intensity;     // Intensity, from 0 to 8
	uint8_t reserved[58];  // Reserved, set to 0xFF
};
_Static_assert(sizeof(struct config) == FLASH_SECTOR_LEN, "invalid config structure length");

// Configuration in flash, read only
static volatile const struct config * const cfg_ro = (volatile struct config*)CFG_ADDR;
// Configuration copy in RAM, read/write
static struct config cfg;
// Current color
static uint32_t color;
// Global event
static enum event evt = EVT_NONE;
// Working mode
static enum mode mod = MODE_NORMAL;

// Callback for the ws2812b send function
uint32_t WS2812BLEDCallback(__attribute__((unused)) int ledno)
{
	return color;
}

// Configures GPIO pins and button interrupts
static void io_setup(void)
{
	// Enable GPIOD and GPIOC
	RCC->APB2PCENR = RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOC |
		RCC_APB2Periph_AFIO;

	// Setup PCB LED and UART TX
	GPIOD->CFGLR =
		((GPIO_CNF_IN_PUPD)<<(4*1)) | // Keep SWIO enabled.
		((GPIO_Speed_2MHz | GPIO_CNF_OUT_PP)<<(4*UART_TX)) |
		((GPIO_Speed_2MHz | GPIO_CNF_OUT_PP)<<(4*LED));
	// Setup buttons
	GPIOC->CFGLR =
		((GPIO_Speed_In | GPIO_CNF_IN_PUPD)<<(4*BUTTON_BLUE)) |
		((GPIO_Speed_In | GPIO_CNF_IN_PUPD)<<(4*BUTTON_ORAN));
	// Enable button pull-ups
	GPIOC->OUTDR = (1<<BUTTON_BLUE) | (1<<BUTTON_ORAN);

	// Enable button interrupts
	EXTI->INTENR = (1<<BUTTON_BLUE) | (1<<BUTTON_ORAN);
	// Fire interrupt on falling both rising and falling edge
	EXTI->FTENR = (1<<BUTTON_BLUE) | (1<<BUTTON_ORAN);
	EXTI->RTENR = (1<<BUTTON_BLUE) | (1<<BUTTON_ORAN);
	// Map interrupts for the buttons on port C (0b10)
	AFIO->EXTICR = (0b10<<(2*BUTTON_BLUE)) | (0b10<<(2*BUTTON_ORAN));

	GPIOD->BSHR = 1<<LED; // LED ON
}

// Configures UART. Call after io_setup()
static void uart_setup(void)
{
	// Enable UART and GPIOD
	RCC->APB2PCENR |= RCC_APB2Periph_GPIOD | RCC_APB2Periph_USART1 |
		RCC_APB2Periph_AFIO;

	// UART TX output on PD0 (PCFR1[21,2] = 0b01)
	// This also changes RX to PD1, but we are not using it
	// (although we should make sure it does not interfere with SWD)
	AFIO->PCFR1 = (AFIO->PCFR1 & ~(1<<21)) | (1<<2);

	// Push-Pull, 10MHz Output on D0, with AutoFunction
	GPIOD->CFGLR = (GPIOD->CFGLR & ~(0xF<<(4*0))) |
			((GPIO_Speed_10MHz | GPIO_CNF_OUT_PP_AF)<<(4*0));

	// Setup UART for Tx 8n1
	USART1->CTLR1 = USART_WordLength_8b | USART_Parity_No | USART_Mode_Tx;
	USART1->CTLR2 = USART_StopBits_1;
	// Enable Tx DMA event
	USART1->CTLR3 = USART_DMAReq_Tx;

	// Set baud rate and enable UART
	USART1->BRR = ((FUNCONF_SYSTEM_CORE_CLOCK) + (UART_BR)/2) / (UART_BR);
	USART1->CTLR1 |= CTLR1_UE_Set;
}

// Configures DMA to use with UART TX. Call after uart_setup()
static void dma_uart_setup(void)
{
	// Enable DMA peripheral
	RCC->AHBPCENR = RCC_AHBPeriph_SRAM | RCC_AHBPeriph_DMA1;

	// Disable channel just in case there is a transfer in progress
	DMA1_Channel4->CFGR &= ~DMA_CFGR1_EN;

	// USART1 TX uses DMA channel 4
	DMA1_Channel4->PADDR = (uint32_t)&USART1->DATAR;
	// MEM2MEM: 0 (memory to peripheral)
	// PL: 0 (low priority since UART is a relatively slow peripheral)
	// MSIZE/PSIZE: 0 (8-bit)
	// MINC: 1 (increase memory address)
	// CIRC: 0 (one shot)
	// DIR: 1 (read from memory)
	// TEIE: 0 (no tx error interrupt)
	// HTIE: 0 (no half tx interrupt)
	// TCIE: 0 (no transmission complete interrupt)
	// EN: 0 (do not enable DMA yet)
	DMA1_Channel4->CFGR = DMA_CFGR1_MINC | DMA_CFGR1_DIR;
}

// Configures timer 2 to use in count down mode with interrupts
static void tim2_setup(void)
{
	// Enable peripheral
	RCC->APB1PCENR |= RCC_APB1Periph_TIM2;
	// Disable timer
	TIM2->CTLR1 = 0;
	// Set prescaler so we have 1 tick per ms
	TIM2->PSC = (FUNCONF_SYSTEM_CORE_CLOCK / 1000) - 1;
	// Force an update event. I don't know why, but if I don't do this,
	// an spurious interrupt will trigger later, when writing to CNT.
	TIM2->SWEVGR = TIM_UG;
	// Clear interrupt caused by the update event
	TIM2->INTFR = 0;
	// Enable TIM2 CH1 update interrupt
	TIM2->DMAINTENR = TIM_UIE;
	// Enable TIM2 interrupts
	NVIC_EnableIRQ(TIM2_IRQn);
}

// Starts timer 2. Will generate an interrupt after delay_ms
static void tim2_launch(uint32_t delay_ms)
{
	// Set counter and start counting down in one pulse mode
	TIM2->CNT = delay_ms;
	TIM2->CTLR1 = TIM_CEN | TIM_DIR | TIM_OPM;
}

// Stops timer 2, preventing it from generating an interrupt
static void tim2_cancel(void)
{
	TIM2->CTLR1 = 0;
}

// Sends data through the UART using DMA
static void dma_uart_tx(const void *data, uint32_t len)
{
	// Disable DMA channel (just in case a transfer is pending)
	DMA1_Channel4->CFGR &= ~DMA_CFGR1_EN;
	// Set transfer length and source address
	DMA1_Channel4->CNTR = len;
	DMA1_Channel4->MADDR = (uint32_t)data;
	// Enable DMA channel to start the transfer
	DMA1_Channel4->CFGR |= DMA_CFGR1_EN;
}

// Adds a checksum to the frame for the YX chip
static void yx_csum_add(union yx_cmd_frame *frame)
{
	uint16_t csum = 0;

	// Add all values from version to param
	for (int i = 1; i <= 6; i++) {
		csum += frame->data[i];
	}
	// Obtain 2's complement
	csum = (~csum) + 1;
	// Write computed value
	frame->csum_h = csum>>8;
	frame->csum_l = csum & 0xFF;
}

// Sends a command to the YX chip
static void yx_cmd_send(uint_fast8_t cmd, uint_fast16_t param)
{
	static union yx_cmd_frame frame = {
		.sof = 0x7E,
		.ver = 0xFF,
		.len = 6,
		.feedback = 0, // No feedback
		.eof = 0xEF
	};

	frame.cmd = cmd;
	frame.param_h = param>>8;
	frame.param_l = param & 0xFF;
	yx_csum_add(&frame);
	dma_uart_tx(frame.data, sizeof(frame.data));
}

// Parses button press event in normal mode
static void button_press(bool orange)
{
	const uint16_t random = SysTick->CNT % 3;
	uint16_t track = FOLDER_GUN;

	if (orange) {
		// Play orange portal fire sound
		track += random + 6;
		color = COLOR_ORAN;
	} else {
		// Play blue portal fire sound
		track += random + 3;
		color = COLOR_BLUE;
	}
	yx_cmd_send(YX_FOLDER_PLAY_CMD, track);
	WS2812BDMAStart(NR_LEDS);
}

// Parses double button press event in normal mode
static void button_double(bool orange)
{
	const uint16_t random = SysTick->CNT;
	uint16_t track;

	if (orange) {
		track = FOLDER_GLADOS;
		track += (random % GLADOS_NFILES) + 1;
	} else {
		track = FOLDER_TURRET;
		track += (random % TURRET_NFILES) + 1;
	}
	yx_cmd_send(YX_FOLDER_PLAY_CMD, track);
}

// Parses button long press in normal mode
static void button_long(bool orange)
{
	// Change color for testing
	if (orange) {
		mod = MODE_CFG_VOLUME;
		color = RGB2U32(255, 0, 0);
	} else {
		mod = MODE_CFG_INTENSITY;
		color = RGB2U32(0, 255, 0);
	}
	WS2812BDMAStart(NR_LEDS);
}

// Normal mode event parser
static void normal_event_proc(enum event evt)
{
	switch (evt)
	{
	case EVT_BUTTON_BLUE_PRESS:
		// fallthrough
	case EVT_BUTTON_ORANGE_PRESS:
		tim2_launch(CFG_MODE_DELAY_MS);
		button_press(evt & 1);
		break;

	case EVT_BUTTON_BLUE_RELEASE:
		// fallthrough
	case EVT_BUTTON_ORANGE_RELEASE:
		tim2_cancel();
		break;

	case EVT_BUTTON_BLUE_DOUBLE:
		// fallthrough
	case EVT_BUTTON_ORANGE_DOUBLE:
		tim2_cancel();
		button_double(evt & 1);
		break;

	case EVT_BUTTON_BLUE_LONG:
		//falthrough
	case EVT_BUTTON_ORANGE_LONG:
		button_long(evt & 1);
		break;

	default:
		return;
	}
}

// Changes volume
static void volume_change(bool decrease)
{
	bool change = false;

	if (decrease) {
		if (cfg.volume > 0) {
			cfg.volume--;
			change = true;
		}
	} else {
		if (cfg.volume < 30) {
			cfg.volume++;
			change = true;
		}
	}

	if (change) {
		yx_cmd_send(YX_VOL_SET_CMD, cfg.volume);
		// Polling in interrupt context is not nice, but since we are
		// not updating the lights here, it should worl
		Delay_Ms(VOL_SET_DELAY_MS);
		yx_cmd_send(YX_FOLDER_PLAY_CMD, FOLDER_GUN + 2);
	}
}

// Saves config and returns to normal mode
static void normal_mode_return(void)
{
	// Save config and return to normal mode
	flash_quick_replace(CFG_ADDR, (uint8_t*)&cfg);
	color = COLOR_BLUE;
	WS2812BDMAStart(NR_LEDS);
	mod = MODE_NORMAL;
}

// Event parser during volume cfg mode
static void cfg_volume_event_proc(enum event evt)
{
	switch (evt) {
	case EVT_BUTTON_BLUE_PRESS:
		// falthrough
	case EVT_BUTTON_ORANGE_PRESS:
		volume_change(evt & 1);
		break;

	case EVT_BUTTON_BLUE_DOUBLE:
		// fallthrough
	case EVT_BUTTON_ORANGE_DOUBLE:
		normal_mode_return();
		break;
	default:
		break;
	}
}

// Changes LED intensity
static void intensity_change(bool decrease)
{
	if (decrease) {
		if (cfg.intensity > 0) {
			cfg.intensity--;
		}
	} else {
		if (cfg.intensity < 8) {
			cfg.intensity++;
		}
	}
	color = RGB2U32(0, 255, 0);
	WS2812BDMAStart(NR_LEDS);
}

// Event parser during intensity cfg mode
static void cfg_intensity_event_proc(enum event evt)
{
	switch (evt) {
	case EVT_BUTTON_BLUE_PRESS:
		// falthrough
	case EVT_BUTTON_ORANGE_PRESS:
		intensity_change(evt & 1);
		break;

	case EVT_BUTTON_BLUE_DOUBLE:
		// fallthrough
	case EVT_BUTTON_ORANGE_DOUBLE:
		normal_mode_return();
		break;
	default:
		break;
	}
}

// Global event parser
static void event_proc(enum event evt)
{
	if (MODE_NORMAL == mod) {
		normal_event_proc(evt);
	} else if (MODE_CFG_VOLUME == mod) {
		cfg_volume_event_proc(evt);
	} else {
		cfg_intensity_event_proc(evt);
	}
}

// Timer 2 interrupt, used to detect the button hold condition
__attribute__((interrupt)) void TIM2_IRQHandler(void)
{
	// Event depends on which button (blue/orange) caused timer 2 to launch
	if (EVT_BUTTON_BLUE_PRESS == evt) {
		evt = EVT_BUTTON_BLUE_LONG;
	} else if (EVT_BUTTON_ORANGE_PRESS == evt) {
		evt = EVT_BUTTON_ORANGE_LONG;
	} else {
		evt = EVT_NONE;
	}

	event_proc(evt);
	TIM2->INTFR = 0;
}

// Orange/Blue switch press interrupt
__attribute__((interrupt)) void EXTI7_0_IRQHandler(void)
{
	const uint32_t iostat = GPIOC->INDR;
	const uint32_t intstat = EXTI->INTFR;

	if (intstat & (1<<BUTTON_ORAN)) {
		if (iostat & (1<<BUTTON_ORAN)) {
			evt = EVT_BUTTON_ORANGE_RELEASE;
		} else {
			if (KEY_PRESSED(BUTTON_BLUE)) {
				evt = EVT_BUTTON_ORANGE_DOUBLE;
			} else {
				evt = EVT_BUTTON_ORANGE_PRESS;
			}
		}
		event_proc(evt);
	}

	if (intstat & (1<<BUTTON_BLUE)) {
		if (iostat & (1<<BUTTON_BLUE)) {
			evt = EVT_BUTTON_BLUE_RELEASE;
		} else {
			if (KEY_PRESSED(BUTTON_ORAN)) {
				evt = EVT_BUTTON_BLUE_DOUBLE;
			} else {
				evt = EVT_BUTTON_BLUE_PRESS;
			}
		}
		event_proc(evt);
	}

	// Clear flags
	EXTI->INTFR = intstat;
}

// Yeah, sets default configuration
static void cfg_default_set(void)
{
	memset(&cfg, 0xFF, sizeof(struct config));
	cfg.magic = CFG_MAGIC;
	cfg.volume = CFG_VOLUME_DEFAULT;
	cfg.intensity = CFG_INTENSITY_DEFAULT;

	flash_quick_replace(CFG_ADDR, (uint8_t*)&cfg);
}

// Reads stored configuration, or sets default one if there was none
static void cfg_init(void)
{
	if (cfg_ro->magic != CFG_MAGIC) {
		cfg_default_set();
	} else {
		memcpy(&cfg, (void*)cfg_ro, sizeof(struct config));
	}
}

// Entry point
int main(void)
{
	SystemInit();
	io_setup();
	tim2_setup();
	uart_setup();
	dma_uart_setup();
	WS2812BDMAInit();
	cfg_init();

	// Perform first color update
	color = COLOR_BLUE;
	WS2812BDMAStart(NR_LEDS);
	// Wait for DFPlayer to power up and play startup sound.
	// NOTE: This takes into account the ~525 ms delay in the bootloader
	// waiting for USB host, for a total 1s delay. If you are not using
	// the bootloader, or are using it without delay, you might need
	// increasing this to 1000 ms.
	Delay_Ms(475);
	yx_cmd_send(YX_VOL_SET_CMD, cfg.volume);
	Delay_Ms(VOL_SET_DELAY_MS);
	yx_cmd_send(YX_FOLDER_PLAY_CMD, FOLDER_GUN + 1);

	// Clear flags and enable EXTI interrupts to process button presses
	EXTI->INTFR = EXTI->INTFR;
	NVIC_EnableIRQ(EXTI7_0_IRQn);

	while (1) {
		Delay_Ms(20); // Allow update to take effect before sleep
		// Just sleep. Interrupts/DMA will do the work
		__WFE();
		// Toggle PCB LED (for debugging)
		GPIOD->OUTDR ^= 1<<LED;
	}

	return 0;
}
