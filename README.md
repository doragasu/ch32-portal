# Introduction

This firmware implements lighting and sound for a [Portal Gun](https://www.printables.com/model/922005-portal-gun-v2-with-sound) I printed. The controller board is a [nanoCH32V003](https://github.com/wuxx/nanoCH32V003) with the infamous "10 cent RISC-V MCU" CH32V003. It is connected to a DFPlayer Mini to play sounds, and to a total of 20 WS2812B RGB LEDs.

The firmware uses [ch32v003fun framework from cnlohr](https://github.com/cnlohr/ch32v003fun) that as its name suggests, is really fun to use. Check it out!

# Usage

The firmware supports three working modes:

1. Normal mode: the default when you start the gun, plays portal sounds and changes light colors.
2. Volume configuration mode: allows configuring the sound volume.
3. Intensity configuration mode: allows configuring light intensity with 8 different values plus completely off.

Depending on which mode the firmware is in, the two external buttons perform different actions.

## Normal mode

In normal mode, the leds stay in blue or orange color depending on the last pressed button. Pressing buttons performs the following actions:

* Pressing RED button changes lights to orange and randomly plays one of three orange portal fire sounds.
* Pressing BLUE button changes lights to blue and randomly plays one of three blue portal fire sounds.
* Pressing RED button while holding BLUE button, randomly plays a GLaDOS sound.
* Pressing BLUE button while holding RED button, randomly plays a turret sound.
* Keeping the RED button pressed for at least 5 seconds, enters volume configuration mode.
* Keeping the BLUE button pressed for at least 5 seconds, enters intensity configuraiton mode.

## Volume configuration mode

While in volume configuration mode, the device lights stay in red color, and the buttons do the following:

* Pressing RED button decreases volume and plays a sound with the current volume.
* Pressing BLUE button increases volume and plays a sound with the current volume.
* Pressing both buttons simultaneously saves the configured volume and returns to normal mode.

## Intensity configuration mode

While in intensity configuration mode, the device lights stay in green color, and the buttons do the following:

* Pressing RED button decreases light intensity.
* Pressing BLUE button light intensity.
* Pressing both buttons simultaneously saves the intensity configuration and returns to normal mode.

# Assembly

## Bill of Materials

| Qty | Part | Details |
|-----|------|---------|
| 1   | [16 x WS2812B LED ring](https://aliexpress.com/item/1005002287819725.html) | 72 mm outer diameter |
| 4   | [Single WS2812B LED](https://aliexpress.com/item/32789400837.html) | Can also be cut from a [LED strip](https://aliexpress.com/item/2036819167.html) |
| 1   | [nanoCH32V003 board](https://aliexpress.com/item/1005007067341796.html) | This is the controller board |
| 1   | [DFPlayer Mini](https://aliexpress.com/item/1005006181511315.html) | Used to play sounds |
| 1   | [41 x 71 speaker](https://aliexpress.com/item/1005004845421152.html) | Must be 3W or more |
| 1   | [Red pushbutton](https://aliexpress.com/item/1005005906389808.html) | Could have preferred orange, but could not find one |
| 1   | [Blue pushbutton](https://aliexpress.com/item/1005005906389808.html) | Same as previous one, just change color |
| 1   | [MTS-202 switch](https://aliexpress.com/item/32871917365.html) | A DPDT switch for power on/off |
| 1   | [EMTEC 10000 mAh Power Bank](https://www.emtec-international.com/en/mobility/power-banks/u800-harry-potter) | No need for it to be Harry Potter themed 😅. Mine was bought at a local store and wasn't |
| 2   | 10 uF capacitor | I used through hole electrolytic capacitors to solder them directly to the buttons, but can use whatever capacitor type you want |
| 1   | MicroSD card | Only about 90 MiB storage are required, so whatever one you have lying around will work. Must be plugged to the DFPlayer Mini |

## Wiring

LEDs must be connected as explained in the [Portal Gun thingiverse post](https://www.thingiverse.com/thing:6191828). Once done, wiring is shown in the following diagram (note that for simplicity, the whole LED chain is represented by a single LED in the diagram):

![Wiring Diagram](doc/gun-wiring.svg)

Most connections are straightforward, with the exception of the positive power rail that requires a [DPDT switch](https://learn.sparkfun.com/tutorials/button-and-switch-basics/poles-and-throws-open-and-closed) to achieve the following:

* In switch position 1, power coming from the nanoCH32V003 USB port is routed to the battery input port. No power is routed to the LEDs and DFPlayer Mini.
* In switch position 2, power coming from the battery output port is routed to the nanoCH32V003 board, the WS2812B LEDs and the DFPlayer Mini.

This way, in position 1, the gun is OFF, but if you plug a charger to the nanoCH32V003 board, the power is routed to the Power Bank and the battery is charged. If switch position 2 is selected, the Power Bank powers everything from its output port. When the switch is in position 2 (Gun ON), it is not recommended having a charger connected to the nanoCH32V003 board.

### Power Bank and Power Switch

1. Connect the Power Bank positive **output** terminal to the right of both poles of the switch.
2. Connect the Power Bank positive **input** terminal to the left of the first pole of the switch (the left of the second pole stays unconnected).
3. In the Power Bank, connect the negative terminal of both the **output** and **input** ports.
4. Connect the negative terminal of any Power Bank port to the nanoCH32V003 board `G` pin (either pin 1 or pin 22), to the `GND` pin of the WS2812B LED strip, to the DFPlayer Mini `GND` (pin 7) and to one pin of each pushbutton.
5. Connect the center of the first pole of the switch to the nanoCH32V003 board `5V` pin.
6. Connect the center of the second pole of the switch to the WS2812B LED strip `5V` and the DFPlayer Mini `VCC` (Pin 1).

### LEDs

2. Connect LEDs `DIN` to `C6` on the nanoCH32V003 board.

### Pushbuttons

1. Connect the still unused pin of the RED pushbutton to `C3` on the nanoCH32V003.
2. Connect the still unused pin of the BLUE pushbutton to `C4` on the nanoCH32V003.
3. Put a 10 uF capacitor in parallel with each pushbutton (for debouncing). If the capacitor has polarity, make sure the negative pin is connected to GND.

### DFPlayer Mini

2. Connect `RX` (Pin 2) on the DFPlayer Mini to `D0` on the nanoCH32V003.
3. Connect a speaker (3W or more) to `SPK_1` and `SPK_2` (pins 6 and 8) on the DFPlayer Mini.

### (Optional) USB bootloader

Although USB is not necessary for the Portal Gun to work at all, using USB can be convenient if you want to update the CH32V003 firmware without having to open the gun enclosure. Unfortunately, the nanoCH32V003 board does not have the `D+` and `D-` pins populated, so if you want to use the USB, you will have to mod the board, as shown in the photograph:

![USB mod for rv003usb](doc/usb-mod.jpg)

1. Connect `D3` pin to the USB `D+` USB signal.
1. Connect `D4` pin to the USB `D-` USB signal.
2. Solder a 1.5k resistor between `D4` and `D5`.

After doing this mod, you can burn [rv003usb bootloader](https://github.com/cnlohr/rv003usb). My recommendation is that you configure it to wait 525 ms and boot to application if no host activity is detected in this interval. To do so, in `rv003usb` code, edit `bootloader/bootloader.c` and make sure the following defines are like this:

```C
#define BOOTLOADER_TIMEOUT_PWR 7
#define BOOTLOADER_TIMEOUT_USB 0
```

**NOTE**: If you do **not** use the bootloader, you might have to compensate for the wait time. If your configured volume is not properly set on startup and/or the startup sound does not play, edit `ch32-portal.c` file and in the `main()` function at the bottom of the file increase the `Delay_Ms(450);` to `Delay_Ms(1000)`.

## microSD Card

You need a microSD card with the sound files that will be played. I cannot include in this repository the game sounds since they are copyrighted by Valve Software. You can extract them yourself from your Portal/Portal 2 copy using tools like [VPKEdit](https://github.com/craftablescience/VPKEdit). DFPlayer Mini uses exclusively the number on the beginning of the file/folder for the playback function, so files must start with the correct number, followed by whatever you want. So format a microSD card with FAT32 filesystem and add the following layout:

```
.
├── 01
│   ├── 001_wpn_portalgun_activation_01.wav: Played when the board is powered on
│   ├── 002_wpn_portal_ambient_lp_01.wav: Currently unused
│   ├── 003_wpn_portal_gun_fire_blue_01.wav: Blue portal fire (three variants)
│   ├── 004_wpn_portal_gun_fire_blue_02.wav
│   ├── 005_wpn_portal_gun_fire_blue_03.wav
│   ├── 006_wpn_portal_gun_fire_red_01.wav: Orange portal fire (three variants)
│   ├── 007_wpn_portal_gun_fire_red_02.wav
│   └── 008_wpn_portal_gun_fire_red_03.wav
├── 02
│   ├── 001_turret_active_1.wav: Put here turret sounds
│   ├── ...
│   └── 050_turret_tipped_6.wav
└── 03
    ├── 001_00_part1_entry-1.wav: Put here GLaDOS sounds
    ├── ...
    └── 078_testchambermisc41.wav
```

For the turret sounds I used 50 different files, and for GLaDOS sounds I used 78 different files. If you want to use a different number of files, you will have to edit the sources, change `TURRET_NFILES` and `GLADOS_NFILES` to the appropriate number and rebuild/flash the firmware.

Once the microSD is prepared, insert it into the DFPlayer Mini and you are ready to go!

## Powering the system

Both WS2812B LEDs and the DFPlayer Mini (when playing at full volume) are quite power hungry. I power them with a 10 Ah Power Bank. I would recommend that the power source is able to provide at least 1A at 5V.

# Building the sources

You will need a RISC-V bare-metal compiler such as `riscv64-unknown-elf-gcc` on ArchLinux. Once your compiler is installed, just clone the repository recursively and `make`. To flash the firmware, with a proper programmer plugged to the board, run `make flash` and that's all. For more information, read the [ch32v003fun documentation](https://github.com/cnlohr/ch32v003fun/wiki).

Alternatively, you can download a prebuilt firmware from this [repository package registry](https://gitlab.com/doragasu/ch32-portal/-/packages/).

# Author
This program has been written by Jesús Alonso (doragasu).

# Contributions
Contributions are welcome. If you find a bug please open an issue, and if you have implemented a cool feature/improvement, please send a pull request.

# License
This program is provided with NO WARRANTY, under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
