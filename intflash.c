#include "intflash.h"

#include "ch32v003fun.h"

#define BUSY_WAIT() while (FLASH->STATR & FLASH_STATR_BSY)

static void unlock(void)
{
	__disable_irq();
	FLASH->KEYR = 0x45670123;
	FLASH->KEYR = 0xCDEF89AB;

	FLASH->MODEKEYR = 0x45670123;
	FLASH->MODEKEYR = 0xCDEF89AB;
}

static void lock(void)
{
	// FAST_LOCK bit (15) is not exported for the CH32V003 by ch32v003fun.h,
	// but according to documentation, it is supported.
	FLASH->CTLR = FLASH_CTLR_LOCK | (1<<15);
	__enable_irq();
}

static void quick_erase(uint32_t addr)
{
	FLASH->CTLR = CR_PAGE_ER; // FTER, erase a 64-byte page
	FLASH->ADDR = addr;
	FLASH->CTLR = CR_STRT_Set | CR_PAGE_ER;
	BUSY_WAIT();

	FLASH->CTLR = 0;
}

static void quick_program(uint32_t addr, const uint8_t data[FLASH_SECTOR_LEN])
{
	volatile uint32_t *dst = (volatile uint32_t*)addr;
	uint32_t *src = (uint32_t*)data;
	FLASH->CTLR = CR_PAGE_PG;  // FTPG, quick program 64-byte page
	FLASH->CTLR = CR_BUF_RST | CR_PAGE_PG;
	FLASH->ADDR = addr;
	BUSY_WAIT();

	// Write data to buffer (must point to appropriate address
	for (uint_fast8_t i = 0; i < (FLASH_SECTOR_LEN / sizeof(uint32_t)); i++) {
		*dst++ = *src++;
		FLASH->CTLR = CR_PAGE_PG | FLASH_CTLR_BUF_LOAD;
		BUSY_WAIT();
	}

	// Perform buffer to flash program
	FLASH->CTLR = CR_PAGE_PG|CR_STRT_Set;
	BUSY_WAIT();
}

void flash_quick_erase(uint32_t addr)
{
	unlock();
	quick_erase(addr);
	lock();
}

void flash_quick_program(uint32_t addr, const uint8_t data[FLASH_SECTOR_LEN])
{
	unlock();
	quick_program(addr, data);
	lock();
}

void flash_quick_replace(uint32_t addr, const uint8_t data[FLASH_SECTOR_LEN])
{
	unlock();
	quick_erase(addr);
	quick_program(addr, data);
	lock();
}
